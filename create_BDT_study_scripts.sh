#!/bin/bash
#NFIT=1000

CUT=${2?Error: cut not given}
NAMEFOLDER=${3?Error: dir not given}

echo "Generating scripts for submission"
SCRIPT_IN=${SCRIPT%.*}
SCRIPTFOLD=./scripts_to_submit/main${NAMEFOLDER}/
mkdir -p $SCRIPTFOLD
mkdir -p $SCRIPTFOLD/logs

cp base_to_submit.slurm $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm

echo "cd ">> $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm

echo 'echo "Cut:   ' $CUT '"'   >> $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm
echo 'echo "Folder:' $NAMEFOLDER '"'   >> $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm

echo "cd /home/hep/davide/RKHighq2/sensitivity_studies/" >> $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm

echo 'python sensitivity_studies_v1.py ' $CUT $NAMEFOLDER >> $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm
# echo './bin/superMain1Run2 ' $TRIGCAT $LEPTON $YEAR >> $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${LEPTON}.slurm
# echo 'cp ' $SCRIPT $SCRIPTFOLD
echo 'conda deactivate' >> $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm
echo 'echo "== The End =="' >> $SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm
DIR=/home/hep/davide/RKHighq2/sensitivity_studies/
cd $DIR/$SCRIPTFOLD/logs
sbatch  $DIR/$SCRIPTFOLD/to_submit_${SCRIPT_IN}_${NAMEFOLDER}.slurm
cd $DIR


