2021-03-01 23:22:32.408840: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcudart.so.11.0'; dlerror: libcudart.so.11.0: cannot open shared object file: No such file or directory
2021-03-01 23:22:32.408888: I tensorflow/stream_executor/cuda/cudart_stub.cc:29] Ignore above cudart dlerror if you do not have a GPU set up on your machine.
2021-03-01 23:22:39.604127: I tensorflow/compiler/jit/xla_cpu_device.cc:41] Not creating XLA devices, tf_xla_enable_xla_devices not set
2021-03-01 23:22:39.606875: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcuda.so.1'; dlerror: libcuda.so.1: cannot open shared object file: No such file or directory
2021-03-01 23:22:39.606895: W tensorflow/stream_executor/cuda/cuda_driver.cc:326] failed call to cuInit: UNKNOWN ERROR (303)
2021-03-01 23:22:39.606915: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:156] kernel driver does not appear to be running on this host (farm-ui2): /proc/driver/nvidia/version does not exist
2021-03-01 23:22:39.607265: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-03-01 23:22:39.610032: I tensorflow/compiler/jit/xla_gpu_device.cc:99] Not creating XLA devices, tf_xla_enable_xla_devices not set
***********************
Setting up preselection
BDT cut:       BDT_score_selection>0.96871133
q2cut:J_psi_1S_TRACK_M*J_psi_1S_TRACK_M > 14000000.0
						  
***********************
*********************************
Getting the signal MC parameters
*********************************
*********************************
Building the signal pdf
*********************************
*********************************
Fitting the signal PDF
*********************************
2021-03-01 23:22:46.373868: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:116] None of the MLIR optimization passes are enabled (registered 2)
2021-03-01 23:22:46.435461: I tensorflow/core/platform/profile_utils/cpu_utils.cc:112] CPU Frequency: 3200340000 Hz
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
W MnPosDef Matrix forced pos-def by adding to diagonal 0.452493
W MnPosDef Matrix forced pos-def by adding to diagonal 1.05908
W MnPosDef Matrix forced pos-def by adding to diagonal 1.69308
W MnPosDef Matrix forced pos-def by adding to diagonal 1.95927
W MnPosDef Matrix forced pos-def by adding to diagonal 0.224943
W MnPosDef non-positive diagonal element in covariance matrix[ 9 ] = -31.5098
W MnPosDef Added to diagonal of Error matrix a value 32.0098
W MnPosDef Matrix forced pos-def by adding to diagonal 4.70344
W MnHesse Maximum number of allowed function calls exhausted; will return diagonal matrix
W VariableMetricBuilder Invalid Hessian - exit the minimization
W MnPosDef Matrix forced pos-def by adding to diagonal 0.537137
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def, gdel = 0.000690926 > 0
W MnPosDef non-positive diagonal element in covariance matrix[ 4 ] = -4.58965
W MnPosDef Added to diagonal of Error matrix a value 5.08966
W VariableMetricBuilder gdel = -27.8764
W MnPosDef Matrix forced pos-def by adding to diagonal 0.604783
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def, gdel = 0.0213977 > 0
W MnPosDef non-positive diagonal element in covariance matrix[ 2 ] = -0.000456341
W MnPosDef non-positive diagonal element in covariance matrix[ 4 ] = -12.5313
W MnPosDef Added to diagonal of Error matrix a value 13.0313
W VariableMetricBuilder gdel = -222.931
W MnPosDef Matrix forced pos-def by adding to diagonal 0.475726
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def, gdel = 12.819 > 0
W MnPosDef non-positive diagonal element in covariance matrix[ 2 ] = -0.0446092
W MnPosDef non-positive diagonal element in covariance matrix[ 9 ] = -0.000931731
W MnPosDef Added to diagonal of Error matrix a value 0.54461
W VariableMetricBuilder gdel = -31377.4
/home/hep/davide/RKHighq2/sensitivity_studies/utils/plotFunctions.py:27: UserWarning: The function <function Space.limit1d at 0x7f8a8981aca0> may does not return the actual area/limits but rather the rectangular limits. <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))> can also have functional limits that are arbitrarily defined and lay inside the rect_limits. To test if a value is inside, use `inside` or `filter`.
  lower, upper = data.data_range.limit1d
/home/hep/davide/RKHighq2/sensitivity_studies/utils/plotFunctions.py:30: UserWarning: The function <function Space.limits at 0x7f8a89816670> may does not return the actual area/limits but rather the rectangular limits. <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))> can also have functional limits that are arbitrarily defined and lay inside the rect_limits. To test if a value is inside, use `inside` or `filter`.
  if(data.space.limits!=model.space.limits): scale2=model.integrate(data.space.limits)
WARNING:tensorflow:5 out of the last 5 calls to <function crystalball_func at 0x7f88ac142c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:6 out of the last 6 calls to <function crystalball_func at 0x7f88ac142c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
*********************************
Fitting the Kstee background PDF
*********************************
*********************************************
Fitting the combinatorial upper mass sideband
*********************************************
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/core/loss.py:154: AdvancedFeatureWarning: Either you're using an advanced feature OR causing unwanted behavior. To turn this warning off, use `zfit.settings.advanced_warnings['inconsistent_fitrange']` = False`  or 'all' (use with care) with `zfit.settings.advanced_warnings['all'] = False
PDFs [<zfit.Exponential  params=[lambda] dtype=float64>0] as well as `data` [<zfit.core.data.Data object at 0x7f877f1397c0>] have different ranges [(<zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[5400.]]), array([[6200.]]))>, <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))>)] they are defined in. The data range will cut the data while the norm range defines the normalization.
  warn_advanced_feature(f"PDFs {non_consistent['model']} as "
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
***********************************************
Performing efficiency calculations to est yields
***********************************************
*********************************************************************
Done! Generating the toy dataset and ploting the resulting total pdf
*********************************************************************
***********************************************
Setting up the mass fit to the toy dataset     
***********************************************
***********************************************
Frac rare prc = 0.2936818674650962
***********************************************
***********************************************
Fitting the toy dataset
***********************************************
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
WARNING:tensorflow:From /disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/tensorflow/python/ops/linalg/linear_operator_lower_triangular.py:159: calling LinearOperator.__init__ (from tensorflow.python.ops.linalg.linear_operator) with graph_parents is deprecated and will be removed in a future version.
Instructions for updating:
Do not pass `graph_parents`.  They will  no longer be used.
2021-03-01 23:23:54.362344: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.
2021-03-01 23:23:55.474600: W tensorflow/core/grappler/optimizers/loop_optimizer.cc:906] Skipping loop optimization for Merge node with control input: StatefulPartitionedCall/StatefulPartitionedCall/GaussianConstraint_tfp/scale_matvec_linear_operator/LinearOperatorLowerTriangular/assert_non_singular/assert_no_entries_with_modulus_zero/assert_less/Assert/AssertGuard/branch_executed/_580
W MnPosDef Matrix forced pos-def by adding to diagonal 10.0113
name                 value    at limit
--------------  ----------  ----------
sigYield             760.8       False
combYield               43       False
fracPrc_fit         0.3144       False
frac0gamma_fit       0.485       False
frac1gamma_fit      0.4435       False
lambda_fit      -0.0009806       False
***********************************************
Done! getting the results...                   
***********************************************
***********************************************
Pulls:                                          
***********************************************
sigYield -0.185
combYield 0.979
fracPrc_fit -0.516
frac0gamma_fit 0.091
frac1gamma_fit 0.006
lambda_fit 0.874
WARNING:tensorflow:5 out of the last 23 calls to <function crystalball_func at 0x7f88ac142c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:6 out of the last 24 calls to <function crystalball_func at 0x7f88ac142c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:7 out of the last 25 calls to <function crystalball_func at 0x7f88ac142c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
