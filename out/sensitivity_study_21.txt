2021-03-01 22:32:36.163797: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcudart.so.11.0'; dlerror: libcudart.so.11.0: cannot open shared object file: No such file or directory
2021-03-01 22:32:36.163837: I tensorflow/stream_executor/cuda/cudart_stub.cc:29] Ignore above cudart dlerror if you do not have a GPU set up on your machine.
2021-03-01 22:32:43.219610: I tensorflow/compiler/jit/xla_cpu_device.cc:41] Not creating XLA devices, tf_xla_enable_xla_devices not set
2021-03-01 22:32:43.222152: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcuda.so.1'; dlerror: libcuda.so.1: cannot open shared object file: No such file or directory
2021-03-01 22:32:43.222171: W tensorflow/stream_executor/cuda/cuda_driver.cc:326] failed call to cuInit: UNKNOWN ERROR (303)
2021-03-01 22:32:43.222196: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:156] kernel driver does not appear to be running on this host (farm-ui2): /proc/driver/nvidia/version does not exist
2021-03-01 22:32:43.222491: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-03-01 22:32:43.225201: I tensorflow/compiler/jit/xla_gpu_device.cc:99] Not creating XLA devices, tf_xla_enable_xla_devices not set
***********************
Setting up preselection
BDT cut:       BDT_score_selection>0.83121784
q2cut:J_psi_1S_TRACK_M*J_psi_1S_TRACK_M > 14000000.0
						  
***********************
*********************************
Getting the signal MC parameters
*********************************
*********************************
Building the signal pdf
*********************************
*********************************
Fitting the signal PDF
*********************************
2021-03-01 22:32:49.870711: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:116] None of the MLIR optimization passes are enabled (registered 2)
2021-03-01 22:32:49.935455: I tensorflow/core/platform/profile_utils/cpu_utils.cc:112] CPU Frequency: 3200340000 Hz
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
W MnPosDef Matrix forced pos-def by adding to diagonal 0.480351
W MnPosDef Matrix forced pos-def by adding to diagonal 1.70643
W MnPosDef Matrix forced pos-def by adding to diagonal 2.44307
W MnPosDef Matrix forced pos-def by adding to diagonal 3.06874
W VariableMetricBuilder Reached machine accuracy limit; Edm 0.00112484 is smaller than machine limit 0.00182814 while 0.001 was requested
W MnPosDef Matrix forced pos-def by adding to diagonal 0.114228
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def., try to make pos.def.
W MnPosDef non-positive diagonal element in covariance matrix[ 0 ] = -0.00127201
W MnPosDef non-positive diagonal element in covariance matrix[ 3 ] = -0.000174922
W MnPosDef non-positive diagonal element in covariance matrix[ 5 ] = -0.00201392
W MnPosDef non-positive diagonal element in covariance matrix[ 9 ] = -5.42681e-05
W MnPosDef Added to diagonal of Error matrix a value 0.502015
W MnPosDef Matrix forced pos-def by adding to diagonal 0.0585934
W MnPosDef Matrix forced pos-def by adding to diagonal 0.440909
W MnPosDef non-positive diagonal element in covariance matrix[ 2 ] = -0.126135
W MnPosDef Added to diagonal of Error matrix a value 0.626136
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def, gdel = 0.473239 > 0
W MnPosDef non-positive diagonal element in covariance matrix[ 0 ] = -0.0980425
W MnPosDef non-positive diagonal element in covariance matrix[ 2 ] = -65.0627
W MnPosDef non-positive diagonal element in covariance matrix[ 5 ] = -0.406006
W MnPosDef non-positive diagonal element in covariance matrix[ 9 ] = -0.000455753
W MnPosDef Added to diagonal of Error matrix a value 65.5627
W MnPosDef Matrix forced pos-def by adding to diagonal 0.171126
W VariableMetricBuilder gdel = -9587.98
W MnHesse Maximum number of allowed function calls exhausted; will return diagonal matrix
W VariableMetricBuilder Invalid Hessian - exit the minimization
/home/hep/davide/RKHighq2/sensitivity_studies/utils/plotFunctions.py:27: UserWarning: The function <function Space.limit1d at 0x7f6cae321ca0> may does not return the actual area/limits but rather the rectangular limits. <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))> can also have functional limits that are arbitrarily defined and lay inside the rect_limits. To test if a value is inside, use `inside` or `filter`.
  lower, upper = data.data_range.limit1d
/home/hep/davide/RKHighq2/sensitivity_studies/utils/plotFunctions.py:30: UserWarning: The function <function Space.limits at 0x7f6cae31d670> may does not return the actual area/limits but rather the rectangular limits. <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))> can also have functional limits that are arbitrarily defined and lay inside the rect_limits. To test if a value is inside, use `inside` or `filter`.
  if(data.space.limits!=model.space.limits): scale2=model.integrate(data.space.limits)
WARNING:tensorflow:5 out of the last 5 calls to <function crystalball_func at 0x7f6ad047bc10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:6 out of the last 6 calls to <function crystalball_func at 0x7f6ad047bc10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
*********************************
Fitting the Kstee background PDF
*********************************
*********************************************
Fitting the combinatorial upper mass sideband
*********************************************
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/core/loss.py:154: AdvancedFeatureWarning: Either you're using an advanced feature OR causing unwanted behavior. To turn this warning off, use `zfit.settings.advanced_warnings['inconsistent_fitrange']` = False`  or 'all' (use with care) with `zfit.settings.advanced_warnings['all'] = False
PDFs [<zfit.Exponential  params=[lambda] dtype=float64>0] as well as `data` [<zfit.core.data.Data object at 0x7f69ae03e7c0>] have different ranges [(<zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[5400.]]), array([[6200.]]))>, <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))>)] they are defined in. The data range will cut the data while the norm range defines the normalization.
  warn_advanced_feature(f"PDFs {non_consistent['model']} as "
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
***********************************************
Performing efficiency calculations to est yields
***********************************************
*********************************************************************
Done! Generating the toy dataset and ploting the resulting total pdf
*********************************************************************
***********************************************
Setting up the mass fit to the toy dataset     
***********************************************
***********************************************
Frac rare prc = 0.29319519579336656
***********************************************
***********************************************
Fitting the toy dataset
***********************************************
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
WARNING:tensorflow:From /disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/tensorflow/python/ops/linalg/linear_operator_lower_triangular.py:159: calling LinearOperator.__init__ (from tensorflow.python.ops.linalg.linear_operator) with graph_parents is deprecated and will be removed in a future version.
Instructions for updating:
Do not pass `graph_parents`.  They will  no longer be used.
2021-03-01 22:33:56.998691: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.
2021-03-01 22:33:58.731205: W tensorflow/core/grappler/optimizers/loop_optimizer.cc:906] Skipping loop optimization for Merge node with control input: StatefulPartitionedCall/StatefulPartitionedCall/GaussianConstraint_tfp/scale_matvec_linear_operator/LinearOperatorLowerTriangular/assert_non_singular/assert_no_entries_with_modulus_zero/assert_less/Assert/AssertGuard/branch_executed/_580
W MnPosDef Matrix forced pos-def by adding to diagonal 3.04318
name                value    at limit
--------------  ---------  ----------
sigYield            754.7       False
combYield           175.9       False
fracPrc_fit        0.3255       False
frac0gamma_fit     0.4846       False
frac1gamma_fit     0.4458       False
lambda_fit      -0.002577       False
***********************************************
Done! getting the results...                   
***********************************************
***********************************************
Pulls:                                          
***********************************************
sigYield 1.235
combYield -0.807
fracPrc_fit -0.61
frac0gamma_fit 0.046
frac1gamma_fit 0.019
lambda_fit -0.864
WARNING:tensorflow:5 out of the last 23 calls to <function crystalball_func at 0x7f6ad047bc10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:6 out of the last 24 calls to <function crystalball_func at 0x7f6ad047bc10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:7 out of the last 25 calls to <function crystalball_func at 0x7f6ad047bc10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
