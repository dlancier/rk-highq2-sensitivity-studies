2021-03-01 22:48:11.505278: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcudart.so.11.0'; dlerror: libcudart.so.11.0: cannot open shared object file: No such file or directory
2021-03-01 22:48:11.505322: I tensorflow/stream_executor/cuda/cudart_stub.cc:29] Ignore above cudart dlerror if you do not have a GPU set up on your machine.
2021-03-01 22:48:18.626913: I tensorflow/compiler/jit/xla_cpu_device.cc:41] Not creating XLA devices, tf_xla_enable_xla_devices not set
2021-03-01 22:48:18.629387: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcuda.so.1'; dlerror: libcuda.so.1: cannot open shared object file: No such file or directory
2021-03-01 22:48:18.629408: W tensorflow/stream_executor/cuda/cuda_driver.cc:326] failed call to cuInit: UNKNOWN ERROR (303)
2021-03-01 22:48:18.629428: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:156] kernel driver does not appear to be running on this host (farm-ui2): /proc/driver/nvidia/version does not exist
2021-03-01 22:48:18.629742: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-03-01 22:48:18.632839: I tensorflow/compiler/jit/xla_gpu_device.cc:99] Not creating XLA devices, tf_xla_enable_xla_devices not set
***********************
Setting up preselection
BDT cut:       BDT_score_selection>0.9326904
q2cut:J_psi_1S_TRACK_M*J_psi_1S_TRACK_M > 14000000.0
						  
***********************
*********************************
Getting the signal MC parameters
*********************************
*********************************
Building the signal pdf
*********************************
*********************************
Fitting the signal PDF
*********************************
2021-03-01 22:48:25.445675: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:116] None of the MLIR optimization passes are enabled (registered 2)
2021-03-01 22:48:25.507467: I tensorflow/core/platform/profile_utils/cpu_utils.cc:112] CPU Frequency: 3200340000 Hz
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
W MnPosDef Matrix forced pos-def by adding to diagonal 0.460893
W MnPosDef Matrix forced pos-def by adding to diagonal 0.570247
W MnPosDef Matrix forced pos-def by adding to diagonal 2.03011
W MnPosDef Matrix forced pos-def by adding to diagonal 2.52173
W VariableMetricBuilder Reached machine accuracy limit; Edm 0.00166247 is smaller than machine limit 0.00176033 while 0.001 was requested
W MnPosDef Matrix forced pos-def by adding to diagonal 0.111995
W MnPosDef Matrix forced pos-def by adding to diagonal 0.293922
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def., try to make pos.def.
W MnPosDef non-positive diagonal element in covariance matrix[ 9 ] = -0.000226766
W MnPosDef Added to diagonal of Error matrix a value 0.500228
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def, gdel = 0.307785 > 0
W MnPosDef non-positive diagonal element in covariance matrix[ 1 ] = -0.0608108
W MnPosDef non-positive diagonal element in covariance matrix[ 2 ] = -0.538311
W MnPosDef non-positive diagonal element in covariance matrix[ 3 ] = -0.00100784
W MnPosDef non-positive diagonal element in covariance matrix[ 4 ] = -50.5908
W MnPosDef non-positive diagonal element in covariance matrix[ 5 ] = -1.86227
W MnPosDef non-positive diagonal element in covariance matrix[ 9 ] = -0.00069872
W MnPosDef Added to diagonal of Error matrix a value 51.0908
W MnPosDef Matrix forced pos-def by adding to diagonal 1.37686
W VariableMetricBuilder gdel = -196792
W VariableMetricBuilder No improvement in line search
W VariableMetricBuilder Iterations finish without convergence; Edm 1.28518 Requested 0.001
W MnPosDef non-positive diagonal element in covariance matrix[ 4 ] = -0.11005
W MnPosDef Added to diagonal of Error matrix a value 0.610051
W MnPosDef Matrix forced pos-def by adding to diagonal 0.0181622
/home/hep/davide/RKHighq2/sensitivity_studies/utils/plotFunctions.py:27: UserWarning: The function <function Space.limit1d at 0x7f1c2b965ca0> may does not return the actual area/limits but rather the rectangular limits. <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))> can also have functional limits that are arbitrarily defined and lay inside the rect_limits. To test if a value is inside, use `inside` or `filter`.
  lower, upper = data.data_range.limit1d
/home/hep/davide/RKHighq2/sensitivity_studies/utils/plotFunctions.py:30: UserWarning: The function <function Space.limits at 0x7f1c2b961670> may does not return the actual area/limits but rather the rectangular limits. <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))> can also have functional limits that are arbitrarily defined and lay inside the rect_limits. To test if a value is inside, use `inside` or `filter`.
  if(data.space.limits!=model.space.limits): scale2=model.integrate(data.space.limits)
WARNING:tensorflow:5 out of the last 5 calls to <function crystalball_func at 0x7f1a40293c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:6 out of the last 6 calls to <function crystalball_func at 0x7f1a40293c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
*********************************
Fitting the Kstee background PDF
*********************************
*********************************************
Fitting the combinatorial upper mass sideband
*********************************************
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/core/loss.py:154: AdvancedFeatureWarning: Either you're using an advanced feature OR causing unwanted behavior. To turn this warning off, use `zfit.settings.advanced_warnings['inconsistent_fitrange']` = False`  or 'all' (use with care) with `zfit.settings.advanced_warnings['all'] = False
PDFs [<zfit.Exponential  params=[lambda] dtype=float64>0] as well as `data` [<zfit.core.data.Data object at 0x7f19112277c0>] have different ranges [(<zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[5400.]]), array([[6200.]]))>, <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))>)] they are defined in. The data range will cut the data while the norm range defines the normalization.
  warn_advanced_feature(f"PDFs {non_consistent['model']} as "
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
***********************************************
Performing efficiency calculations to est yields
***********************************************
*********************************************************************
Done! Generating the toy dataset and ploting the resulting total pdf
*********************************************************************
***********************************************
Setting up the mass fit to the toy dataset     
***********************************************
***********************************************
Frac rare prc = 0.2897433724100809
***********************************************
***********************************************
Fitting the toy dataset
***********************************************
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
WARNING:tensorflow:From /disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/tensorflow/python/ops/linalg/linear_operator_lower_triangular.py:159: calling LinearOperator.__init__ (from tensorflow.python.ops.linalg.linear_operator) with graph_parents is deprecated and will be removed in a future version.
Instructions for updating:
Do not pass `graph_parents`.  They will  no longer be used.
2021-03-01 22:49:20.615083: W tensorflow/python/util/util.cc:348] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.
2021-03-01 22:49:22.351787: W tensorflow/core/grappler/optimizers/loop_optimizer.cc:906] Skipping loop optimization for Merge node with control input: StatefulPartitionedCall/StatefulPartitionedCall/GaussianConstraint_tfp/scale_matvec_linear_operator/LinearOperatorLowerTriangular/assert_non_singular/assert_no_entries_with_modulus_zero/assert_less/Assert/AssertGuard/branch_executed/_580
W MnPosDef Matrix forced pos-def by adding to diagonal 0.336697
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def., try to make pos.def.
W MnPosDef non-positive diagonal element in covariance matrix[ 1 ] = -0.00311792
W MnPosDef Added to diagonal of Error matrix a value 0.503119
name                value    at limit
--------------  ---------  ----------
sigYield            811.6       False
combYield           56.33       False
fracPrc_fit        0.2871       False
frac0gamma_fit     0.4825       False
frac1gamma_fit     0.4473       False
lambda_fit      -0.002316       False
***********************************************
Done! getting the results...                   
***********************************************
***********************************************
Pulls:                                          
***********************************************
sigYield -0.549
combYield 0.656
fracPrc_fit 0.064
frac0gamma_fit 0.063
frac1gamma_fit 0.007
lambda_fit -0.074
WARNING:tensorflow:5 out of the last 23 calls to <function crystalball_func at 0x7f1a40293c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:6 out of the last 24 calls to <function crystalball_func at 0x7f1a40293c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:7 out of the last 25 calls to <function crystalball_func at 0x7f1a40293c10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
