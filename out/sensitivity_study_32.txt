2021-03-01 22:49:38.260727: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcudart.so.11.0'; dlerror: libcudart.so.11.0: cannot open shared object file: No such file or directory
2021-03-01 22:49:38.260769: I tensorflow/stream_executor/cuda/cudart_stub.cc:29] Ignore above cudart dlerror if you do not have a GPU set up on your machine.
2021-03-01 22:49:45.561313: I tensorflow/compiler/jit/xla_cpu_device.cc:41] Not creating XLA devices, tf_xla_enable_xla_devices not set
2021-03-01 22:49:45.563695: W tensorflow/stream_executor/platform/default/dso_loader.cc:60] Could not load dynamic library 'libcuda.so.1'; dlerror: libcuda.so.1: cannot open shared object file: No such file or directory
2021-03-01 22:49:45.563715: W tensorflow/stream_executor/cuda/cuda_driver.cc:326] failed call to cuInit: UNKNOWN ERROR (303)
2021-03-01 22:49:45.563734: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:156] kernel driver does not appear to be running on this host (farm-ui2): /proc/driver/nvidia/version does not exist
2021-03-01 22:49:45.564190: I tensorflow/core/platform/cpu_feature_guard.cc:142] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-03-01 22:49:45.566886: I tensorflow/compiler/jit/xla_gpu_device.cc:99] Not creating XLA devices, tf_xla_enable_xla_devices not set
***********************
Setting up preselection
BDT cut:       BDT_score_selection>0.94102538
q2cut:J_psi_1S_TRACK_M*J_psi_1S_TRACK_M > 14000000.0
						  
***********************
*********************************
Getting the signal MC parameters
*********************************
*********************************
Building the signal pdf
*********************************
*********************************
Fitting the signal PDF
*********************************
2021-03-01 22:49:52.244228: I tensorflow/compiler/mlir/mlir_graph_optimization_pass.cc:116] None of the MLIR optimization passes are enabled (registered 2)
2021-03-01 22:49:52.307465: I tensorflow/core/platform/profile_utils/cpu_utils.cc:112] CPU Frequency: 3200340000 Hz
/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/zfit/util/cache.py:283: VisibleDeprecationWarning: Creating an ndarray from ragged nested sequences (which is a list-or-tuple of lists-or-tuples-or ndarrays with different lengths or shapes) is deprecated. If you meant to do this, you must specify 'dtype=object' when creating the ndarray
  return all(np.equal(self.immutable_representation, other.immutable_representation))
W MnPosDef Matrix forced pos-def by adding to diagonal 0.462138
W MnPosDef Matrix forced pos-def by adding to diagonal 1.72776
W MnPosDef Matrix forced pos-def by adding to diagonal 2.12381
W VariableMetricBuilder Reached machine accuracy limit; Edm 0.00161704 is smaller than machine limit 0.001751 while 0.001 was requested
W MnPosDef Matrix forced pos-def by adding to diagonal 0.130348
W VariableMetricBuilder Reached machine accuracy limit; Edm 0.00178572 is smaller than machine limit 0.00189808 while 0.001 was requested
W MnPosDef Matrix forced pos-def by adding to diagonal 0.469339
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W VariableMetricBuilder Matrix not pos.def, gdel = 132.414 > 0
W MnPosDef non-positive diagonal element in covariance matrix[ 0 ] = -0.857062
W MnPosDef non-positive diagonal element in covariance matrix[ 1 ] = -0.0601272
W MnPosDef non-positive diagonal element in covariance matrix[ 2 ] = -8.36114
W MnPosDef non-positive diagonal element in covariance matrix[ 3 ] = -0.00185984
W MnPosDef non-positive diagonal element in covariance matrix[ 5 ] = -0.00997405
W MnPosDef non-positive diagonal element in covariance matrix[ 6 ] = -0.00181368
W MnPosDef non-positive diagonal element in covariance matrix[ 8 ] = -0.049145
W MnPosDef non-positive diagonal element in covariance matrix[ 9 ] = -0.0334643
W MnPosDef Added to diagonal of Error matrix a value 8.86114
W MnPosDef Matrix forced pos-def by adding to diagonal 0.516617
W VariableMetricBuilder gdel = -902268
W VariableMetricBuilder No improvement in line search
W VariableMetricBuilder Iterations finish without convergence; Edm 87.1213 Requested 0.001
W MnPosDef non-positive diagonal element in covariance matrix[ 4 ] = -0.280177
W MnPosDef Added to diagonal of Error matrix a value 0.780178
W DavidonErrorUpdator delgam < 0 : first derivatives increasing along search line
W DavidonErrorUpdator gvg <= 0 : cannot update - return same matrix
W VariableMetricBuilder Matrix not pos.def., try to make pos.def.
W MnPosDef non-positive diagonal element in covariance matrix[ 6 ] = -0.00355617
W MnPosDef Added to diagonal of Error matrix a value 0.503557
/home/hep/davide/RKHighq2/sensitivity_studies/utils/plotFunctions.py:27: UserWarning: The function <function Space.limit1d at 0x7fda72884ca0> may does not return the actual area/limits but rather the rectangular limits. <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))> can also have functional limits that are arbitrarily defined and lay inside the rect_limits. To test if a value is inside, use `inside` or `filter`.
  lower, upper = data.data_range.limit1d
/home/hep/davide/RKHighq2/sensitivity_studies/utils/plotFunctions.py:30: UserWarning: The function <function Space.limits at 0x7fda72880670> may does not return the actual area/limits but rather the rectangular limits. <zfit Space obs=('B_plus_M',), axes=(0,), limits=(array([[4980.]]), array([[6200.]]))> can also have functional limits that are arbitrarily defined and lay inside the rect_limits. To test if a value is inside, use `inside` or `filter`.
  if(data.space.limits!=model.space.limits): scale2=model.integrate(data.space.limits)
WARNING:tensorflow:5 out of the last 5 calls to <function crystalball_func at 0x7fd89419ec10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
WARNING:tensorflow:6 out of the last 6 calls to <function crystalball_func at 0x7fd89419ec10> triggered tf.function retracing. Tracing is expensive and the excessive number of tracings could be due to (1) creating @tf.function repeatedly in a loop, (2) passing tensors with different shapes, (3) passing Python objects instead of tensors. For (1), please define your @tf.function outside of the loop. For (2), @tf.function has experimental_relax_shapes=True option that relaxes argument shapes that can avoid unnecessary retracing. For (3), please refer to https://www.tensorflow.org/guide/function#controlling_retracing and https://www.tensorflow.org/api_docs/python/tf/function for  more details.
*********************************
Fitting the Kstee background PDF
*********************************
*********************************************
Fitting the combinatorial upper mass sideband
*********************************************
Traceback (most recent call last):
  File "sensitivity_studies_v1.py", line 255, in <module>
    Data=treeData.arrays(library='pd', filter_name=branches)
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/uproot/behaviors/TBranch.py", line 1172, in arrays
    return library.group(output, expression_context, how)
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/uproot/interpretation/library.py", line 932, in group
    return pandas.DataFrame(data=arrays, columns=names)
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/pandas/core/frame.py", line 529, in __init__
    mgr = init_dict(data, index, columns, dtype=dtype)
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/pandas/core/internals/construction.py", line 287, in init_dict
    return arrays_to_mgr(arrays, data_names, index, columns, dtype=dtype)
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/pandas/core/internals/construction.py", line 95, in arrays_to_mgr
    return create_block_manager_from_arrays(arrays, arr_names, axes)
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/pandas/core/internals/managers.py", line 1703, in create_block_manager_from_arrays
    mgr._consolidate_inplace()
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/pandas/core/internals/managers.py", line 993, in _consolidate_inplace
    self.blocks = tuple(_consolidate(self.blocks))
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/pandas/core/internals/managers.py", line 1916, in _consolidate
    merged_blocks = _merge_blocks(
  File "/disk/lhcb_data/davide/miniconda3/envs/zfit_env/lib/python3.8/site-packages/pandas/core/internals/managers.py", line 1942, in _merge_blocks
    new_values = new_values[argsort]
KeyboardInterrupt
