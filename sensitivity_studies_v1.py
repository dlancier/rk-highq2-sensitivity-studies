import os
import uproot as ur
import zfit
from zfit import z
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from utils.getPreselection import *
import pandas as pd
from matplotlib import pylab
from utils.plotFunctions import *
import mplhep
import pickle
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('BDTCut', type=float, help='Bdt cut to use')
parser.add_argument('dir', type=int, help='Folder title')


args = parser.parse_args()
cut=args.BDTCut
foldernum=args.dir

mplhep.set_style('LHCb2')



def getEff(dataFrame, cut):
    
    bdt_cut = "BDT_score_selection>"+str(cut)
    totTruthed=dataFrame['KinWeight_MUTOS2DNTracks'].sum()
    
    data_q2Presel = dataFrame.query(preselStringNoPid+" and "+q2Cut+" and "+massRangeCut+" and "+bdt_cut)
    
    totPresel=(data_q2Presel['PIDWeight_DLL4']*data_q2Presel['KinWeight_MUTOS2DNTracks']).sum()
    
    data_t0=data_q2Presel.query('passTrigCat0==1.')
    
    totTrig0=(data_t0['PIDWeight_DLL4']*data_t0['KinWeight_MUTOS2DNTracks']).sum()
    
    effTrig=totTrig0/totPresel
    effPresel=totPresel/totTruthed
    effTot=effTrig*effPresel
    
    return effTot, effTrig, effPresel



BFKstee=1.166e-6
BFKee=5.657e-7
BFPsi2SKee = 7.89e-3*6.21e-4
effStrip=0.13846
				#sigma_bbX*lumi*fragfrac*BF
NKee_bf_Run1 = (72e-6*3e15*0.4*BFKee)
NKee_bf_Run2 = (144e-6*9e15*0.4*BFKee)
NKee_bf=NKee_bf_Run1+NKee_bf_Run2

year=2018
PATH_MC='/disk/lhcb_data/davide/RKHighq2/tuples/'+str(year)+'/MC/KeeTuples/truthed/'
PATH_Data='/disk/lhcb_data/davide/RKHighq2/tuples/'+str(year)+'/data/KeeTuples/HltTOS/'

obs = zfit.Space('B_plus_M', (4980,6200))


# cut=0.5
q2=14e6
plotdir='/home/hep/davide/public_html/RKHighq2/sensitivity_studies/BDTOptimization/fits/'+str(foldernum)+'/'
os.system("mkdir -p "+plotdir)

"""
Set up preselection

"""

preselStringNoPid=getCommonPreselNoPidCutRun2().replace('&&','and').replace('TMath::Abs','abs').replace('||','or')
preselString=getCommonPreselRun2().replace('&&','and').replace('TMath::Abs','abs').replace('||','or')
preselString=preselString.replace('e_minus_PIDe>3','e_minus_PIDe>4').replace('e_plus_PIDe>3','e_plus_PIDe>4')
HltTOSString = getFullHltRun2().replace('&&','and').replace('||','or').replace('TOS','TOS==1.')

q2Cut = "J_psi_1S_TRACK_M*J_psi_1S_TRACK_M > "+str(q2)
trigCut= "passTrigCat0==1"
massRangeCut= "B_plus_M>4980 and B_plus_M<6200"
massRangeCutExt = "B_plus_M>4600 and B_plus_M<6200"
BDTCut = "BDT_score_selection>"+str(cut)
blindRangeCut="(B_plus_M>5400 and B_plus_M<6200)"

print("***********************")
print("Setting up preselection")
print("BDT cut:       "+ BDTCut)
print("q2cut:"+ q2Cut)
print("						  ")
print("***********************")



print("*********************************")
print("Getting the signal MC parameters")
print("*********************************")


tupleMCName='Kee_'+str(year)+'_truthed_bdt_reduced.root'
fileMC = ur.open(PATH_MC+tupleMCName)
treeMC=fileMC['DecayTree']

KeeMC=treeMC.arrays(library='pd', filter_name=branches)
KeeMC*=1.

KeeMC_CommonPresel = KeeMC.query(preselStringNoPid)
KeeMC_HltTOS = KeeMC_CommonPresel.query(HltTOSString)
KeeMC_trig0 = KeeMC_HltTOS.query(q2Cut+" and "+trigCut+" and "+massRangeCut+ " and "+BDTCut)


KeeMC_trig0photon0 = KeeMC_trig0.query("(e_plus_HasBremAdded + e_minus_HasBremAdded) == 0")
KeeMC_trig0photon1 = KeeMC_trig0.query("(e_plus_HasBremAdded + e_minus_HasBremAdded) == 1")
KeeMC_trig0photon2 = KeeMC_trig0.query("(e_plus_HasBremAdded + e_minus_HasBremAdded) == 2")

f0g=len(KeeMC_trig0photon0)/len(KeeMC_trig0)
f1g=len(KeeMC_trig0photon1)/len(KeeMC_trig0)


B_plus_M0 = zfit.Data.from_numpy(obs=obs, weights=KeeMC_trig0photon0['PIDWeight_DLL4']*KeeMC_trig0photon0['KinWeight_MUTOS2DNTracks'], array=KeeMC_trig0photon0['B_plus_M'].to_numpy())
B_plus_M1 = zfit.Data.from_numpy(obs=obs, weights=KeeMC_trig0photon1['PIDWeight_DLL4']*KeeMC_trig0photon1['KinWeight_MUTOS2DNTracks'], array=KeeMC_trig0photon1['B_plus_M'].to_numpy())
B_plus_M2 = zfit.Data.from_numpy(obs=obs, weights=KeeMC_trig0photon2['PIDWeight_DLL4']*KeeMC_trig0photon2['KinWeight_MUTOS2DNTracks'], array=KeeMC_trig0photon2['B_plus_M'].to_numpy())

print("*********************************")
print("Building the signal pdf")
print("*********************************")

nPhotons=0
mean0 = zfit.Parameter("mean"+str(nPhotons), 5280, 5000, 5500)
sigma0 = zfit.Parameter("sigma"+str(nPhotons), 25, 7, 50)
sigma20 = zfit.Parameter("sigma2"+str(nPhotons), 50, 7, 200)


alpha_left0 = zfit.Parameter("alpha_right_"+str(nPhotons), .1, .01, 2)
alpha_right0 = zfit.Parameter("alpha_left_"+str(nPhotons), -.1, -2, -.01)
n_left0 = zfit.Parameter("n_left_"+str(nPhotons), 2, 7, 20)
n_right0 = zfit.Parameter("n_right_"+str(nPhotons), 2, 7, 20)

fracCB0 = zfit.Parameter("fracCB"+str(nPhotons), 0.66, 0.00001, 0.99999)
cb10 = zfit.pdf.CrystalBall(obs=obs, mu=mean0, sigma=sigma0, alpha=alpha_left0, n=n_left0)
cb20 = zfit.pdf.CrystalBall(obs=obs, mu=mean0, sigma=sigma20, alpha=alpha_right0, n=n_right0)
sigModel0 = zfit.pdf.SumPDF([cb10, cb20], [fracCB0])



nPhotons=1
mean1 = zfit.Parameter("mean"+str(nPhotons), 5280, 5000, 5500)
sigma1 = zfit.Parameter("sigma"+str(nPhotons), 30, 7, 50)
sigma21 = zfit.Parameter("sigma2"+str(nPhotons), 50, 7, 100)


alpha_left1 = zfit.Parameter("alpha_right_"+str(nPhotons), .1, .001, 2)
alpha_right1 = zfit.Parameter("alpha_left_"+str(nPhotons), -.1, -2, -.001)
n_left1 = zfit.Parameter("n_left_"+str(nPhotons), 2, 7, 25)
n_right1 = zfit.Parameter("n_right_"+str(nPhotons), 2, 7, 25)


sigma31 = zfit.Parameter("sigmaGaus3"+str(nPhotons),100, 10 , 2000)
fracGaus1 = zfit.Parameter("fracGaus"+str(nPhotons), 0.50, 0.0001, 0.999)
fracCB1 = zfit.Parameter("fracCB"+str(nPhotons), 0.66, 0.00001, 0.99999)
cb11 = zfit.pdf.CrystalBall(obs=obs, mu=mean1, sigma=sigma1, alpha=alpha_left1, n=n_left1)
cb21 = zfit.pdf.CrystalBall(obs=obs, mu=mean1, sigma=sigma21, alpha=alpha_right1, n=n_right1)
sigGaus1 = zfit.pdf.Gauss(obs=obs, mu=mean1, sigma=sigma31)

sigModelCB1 = zfit.pdf.SumPDF([cb11, cb21], [fracCB1])
sigModel1 = zfit.pdf.SumPDF([sigModelCB1,sigGaus1],[fracGaus1])



nPhotons=2
mean2 = zfit.Parameter("mean"+str(nPhotons), 5280, 5000, 5500)
sigma2 = zfit.Parameter("sigma"+str(nPhotons), 50, 7, 100)
sigma22 = zfit.Parameter("sigma2"+str(nPhotons), 50, 7, 100)

alpha_left2 = zfit.Parameter("alpha_right_"+str(nPhotons), .1, .001, 2)
alpha_right2 = zfit.Parameter("alpha_left_"+str(nPhotons), -.1, -2, -.001)
n_left2 = zfit.Parameter("n_left_"+str(nPhotons), 2, 7, 25)
n_right2 = zfit.Parameter("n_right_"+str(nPhotons), 2, 7, 25)

sigma32 = zfit.Parameter("sigmaGaus3"+str(nPhotons),100, 10 , 2000)
fracGaus2 = zfit.Parameter("fracGaus"+str(nPhotons), 0.50, 0.0001, 0.999)

fracCB2 = zfit.Parameter("fracCB"+str(nPhotons), 0.66, 0.00001, 0.99999)
cb12 = zfit.pdf.CrystalBall(obs=obs, mu=mean2, sigma=sigma2, alpha=alpha_left2, n=n_left2)
cb22 = zfit.pdf.CrystalBall(obs=obs, mu=mean2, sigma=sigma22, alpha=alpha_right2, n=n_right2)
sigGaus2 = zfit.pdf.Gauss(obs=obs, mu=mean2, sigma=sigma32)

sigModelCB2 = zfit.pdf.SumPDF([cb12, cb22], [fracCB2])
sigModel2 = zfit.pdf.SumPDF([sigModelCB2,sigGaus2],[fracGaus2])


nll0 = zfit.loss.UnbinnedNLL(sigModel0, B_plus_M0)
nll1 = zfit.loss.UnbinnedNLL(sigModel1, B_plus_M1)
nll2 = zfit.loss.UnbinnedNLL(sigModel2, B_plus_M2)


print("*********************************")
print("Fitting the signal PDF")
print("*********************************")


minimizer = zfit.minimize.Minuit(use_minuit_grad=True, minimize_strategy=2, verbosity=6)


result0 = minimizer.minimize(nll0)
result1 = minimizer.minimize(nll1)
result2 = minimizer.minimize(nll2)


frac0gamma = zfit.Parameter("frac0gamma", f0g, floating=False)
frac1gamma = zfit.Parameter("frac1gamma", f1g, floating=False)
B_plus_M = zfit.Data.from_numpy(obs=obs, weights=KeeMC_trig0['PIDWeight_DLL4']*KeeMC_trig0['KinWeight_MUTOS2DNTracks'], array=KeeMC_trig0['B_plus_M'].to_numpy())

sigModelTot = zfit.pdf.SumPDF([sigModel0, sigModel1, sigModel2], [frac0gamma, frac1gamma])

plot_model(sigModel0, B_plus_M0, save_plot=True, fileName=plotdir+'plotKeePhot0_BDT'+str(foldernum)+'.png')
plot_model(sigModel1, B_plus_M1, save_plot=True, fileName=plotdir+'plotKeePhot1_BDT'+str(foldernum)+'.png')
plot_model(sigModel2, B_plus_M2, save_plot=True, fileName=plotdir+'plotKeePhot2_BDT'+str(foldernum)+'.png')
plot_model(sigModelTot, B_plus_M, save_plot=True, fileName=plotdir+'plotKeeTot_BDT'+str(foldernum)+'.png')


print("*********************************")
print("Fitting the Kstee background PDF")
print("*********************************")


tupleMCKstName='Kstee_'+str(year)+'_truthed_bdt_reduced.root'
fileMCKst = ur.open(PATH_MC+tupleMCKstName)
treeMCKst=fileMCKst['DecayTree']

KstMC=treeMCKst.arrays(library='pd', filter_name=branches)
KstMC*=1.
KstMC_CommonPresel = KstMC.query(preselStringNoPid)
KstMC_HltTOS = KstMC_CommonPresel.query(HltTOSString)

KstMC_trig0 = KstMC_HltTOS.query(q2Cut+" and "+trigCut+" and "+massRangeCutExt+" and "+BDTCut)
obs_extended = zfit.Space('B_plus_M', (4600, 6200))
B_plus_M_partreco = zfit.Data.from_numpy(obs=obs_extended, weights=KstMC_trig0['PIDWeight_DLL4']*KstMC_trig0['KinWeight_MUTOS2DNTracks'], array=KstMC_trig0['B_plus_M'].to_numpy())
part_reco = zfit.pdf.GaussianKDE1DimV1(obs=obs, data=B_plus_M_partreco, bandwidth='adaptive')

plot_model(part_reco, B_plus_M_partreco, save_plot=True, fileName=plotdir+'plotKstee_BDT'+str(foldernum)+'.png')


print("*********************************************")
print("Fitting the combinatorial upper mass sideband")
print("*********************************************")


tupleDataName='B2Kee_'+str(year)+'_CommonPresel_HltTOS_bdt_reduced.root'
fileData = ur.open(PATH_Data+tupleDataName)
treeData=fileData['DecayTree']
Data=treeData.arrays(library='pd', filter_name=branches)
Data*=1.
Data_CommonPresel=Data.query(preselString)
Data_trig0 = Data_CommonPresel.query(q2Cut+" and "+trigCut+" and "+blindRangeCut+" and "+BDTCut)



obs_right_sideband = zfit.Space('B_plus_M', (5400, 6200))
B_plus_M_data = zfit.Data.from_numpy(obs=obs, array=Data_trig0['B_plus_M'].to_numpy())
lam = zfit.Parameter('lambda', -1e-3, -0.1, 0.001)
comb_bkg = zfit.pdf.Exponential(lam, obs=obs)
comb_bkg_yield = zfit.Parameter('comb_bkg_yield', 2000, 0, 10000, step_size=0.1)
comb_bkg_ext = comb_bkg.create_extended(comb_bkg_yield)

with comb_bkg_ext.set_norm_range(obs_right_sideband):
    nll_sidebands = zfit.loss.ExtendedUnbinnedNLL(comb_bkg_ext, B_plus_M_data)
    minimizer.minimize(nll_sidebands)

plot_model_combi(comb_bkg_ext,B_plus_M_data, save_plot=True, fileName=plotdir+'combi_sideband_BDT'+str(foldernum)+'.png')

est_comb=(comb_bkg_yield/comb_bkg_ext.integrate(limits=(5400,6200))).numpy()[0]

print("***********************************************")
print("Performing efficiency calculations to est yields")
print("***********************************************")

effTotKee, _, _ =getEff(KeeMC, cut)
effTotKstee, _, _ =getEff(KstMC, cut)
# effTotPsi2SKee, _, _ = getEff(Psi2SKeeMC, cut)

NKee=(NKee_bf_Run1*effStrip*0.66*effTotKee)+(NKee_bf_Run2*effStrip*effTotKee)
NKstee=NKee*(BFKstee*effTotKstee)/(BFKee*effTotKee);
Ncomb=comb_bkg_yield.numpy()
Ntot = NKee+NKstee+Ncomb
fracKee=NKee/Ntot
fracKst=NKstee/Ntot

sig_frac = zfit.Parameter('sig_frac', fracKee, 0, 1)
prc_bkg_frac = zfit.Parameter('prc_bkg_frac', fracKst, 0, 1)

shift_gen=-4.84
shift_gen_err=0.46
scale_gen=1.1178
scale_gen_err=0.0056

#shift1gen=
#scale1gen=

#shift2gen=
#scale2gen=

shift = np.random.normal(shift_gen,shift_gen_err)
scale = np.random.normal(scale_gen,scale_gen_err)


val=mean0.value().numpy()
mean0.set_value(val+shift)
val=mean1.value().numpy()
mean1.set_value(val+shift)
val=mean2.value().numpy()
mean2.set_value(val+shift)

val=sigma0.value().numpy()
sigma0.set_value(val*scale)
val= sigma20.value().numpy()
sigma20.set_value(val*scale) 

val=sigma1.value().numpy()
sigma1.set_value(val*scale)
val=sigma21.value().numpy()
sigma21.set_value(val*scale)
val=sigma31.value().numpy()
sigma31.set_value(val*scale)

val=sigma2.value().numpy()
sigma2.set_value(val*scale)
val=sigma22.value().numpy()
sigma22.set_value(val*scale)
val=sigma32.value().numpy()
sigma32.set_value(val*scale)

print("*********************************************************************")
print("Done! Generating the toy dataset and ploting the resulting total pdf")
print("*********************************************************************")


with comb_bkg.set_norm_range([4980,6200]):
    modelTot = zfit.pdf.SumPDF([sigModelTot, part_reco, comb_bkg], [sig_frac, prc_bkg_frac])
    data = modelTot.sample(n=Ntot)

plot_comp_model(modelTot,data, save_plot=True, fileName=plotdir+'plotTotalToy_BDT'+str(foldernum)+'.png')

nTot=Ntot
nSig=0.3*nTot
nCombi=0.5*nTot
nPrc=0.2*nTot    


print("***********************************************")
print("Setting up the mass fit to the toy dataset     ")
print("***********************************************")

meanShift = zfit.Parameter('meanShift', shift_gen, floating=False)
sigmaScale = zfit.Parameter('sigmaScale', scale_gen, floating=False)
# 
# meanShift1 = zfit.Parameter('meanShift1', shift1gen, floating=False)
# sigmaScale1 = zfit.Parameter('sigmaScale1', scale1gen, floating=False)

# meanShift2 = zfit.Parameter('meanShift2', shift2gen, floating=False)
# sigmaScale2 = zfit.Parameter('sigmaScale2', scale2gen, floating=False)



def meanShift_fn(mean, shift):
    return mean+shift
def sigmaScale_fn(sigma, scale):
    return sigma*scale


mean0.floating = False
sigma0.floating = False
sigma20.floating = False

shiftedMean0 = zfit.ComposedParameter("shiftedMean0",meanShift_fn,params=[mean0, meanShift])
scaledSigma0 = zfit.ComposedParameter("scaledSigma0",sigmaScale_fn,params=[sigma0, sigmaScale])
scaledSigma20 = zfit.ComposedParameter("scaledSigma20",sigmaScale_fn,params=[sigma20, sigmaScale])

alpha_left0.floating = False
alpha_right0.floating = False
n_left0.floating = False
n_right0.floating = False
fracCB0.floating = False

cb10_fit = zfit.pdf.CrystalBall(obs=obs, mu=shiftedMean0, sigma=scaledSigma0, alpha=alpha_left0, n=n_left0)
cb20_fit = zfit.pdf.CrystalBall(obs=obs, mu=shiftedMean0, sigma=scaledSigma20, alpha=alpha_right0, n=n_right0)
sigModel0_fit = zfit.pdf.SumPDF([cb10_fit, cb20_fit], [fracCB0])


mean1.floating = False
sigma1.floating = False
sigma21.floating = False

shiftedMean1 = zfit.ComposedParameter("shiftedMean1",meanShift_fn,params=[mean1, meanShift])
scaledSigma1 = zfit.ComposedParameter("scaledSigma1",sigmaScale_fn,params=[sigma1, sigmaScale])
scaledSigma21 = zfit.ComposedParameter("scaledSigma21",sigmaScale_fn,params=[sigma21, sigmaScale])


alpha_left1.floating = False
alpha_right1.floating = False
n_left1.floating = False
n_right1.floating = False

sigma31.floating = False
scaledSigma31 = zfit.ComposedParameter("scaledSigma31",sigmaScale_fn,params=[sigma31, sigmaScale])
fracGaus1.floating = False
fracCB1.floating = False

cb11_fit = zfit.pdf.CrystalBall(obs=obs, mu=shiftedMean1, sigma=scaledSigma1, alpha=alpha_left1, n=n_left1)
cb21_fit = zfit.pdf.CrystalBall(obs=obs, mu=shiftedMean1, sigma=scaledSigma21, alpha=alpha_right1, n=n_right1)
sigGaus1_fit = zfit.pdf.Gauss(obs=obs, mu=shiftedMean1, sigma=scaledSigma31)
sigModelCB1_fit = zfit.pdf.SumPDF([cb11_fit, cb21_fit], [fracCB1])
sigModel1_fit = zfit.pdf.SumPDF([sigModelCB1_fit,sigGaus1_fit],[fracGaus1])



mean2.floating = False
sigma2.floating = False
sigma22.floating = False
shiftedMean2 = zfit.ComposedParameter("shiftedMean2",meanShift_fn,params=[mean2, meanShift])
scaledSigma2 = zfit.ComposedParameter("scaledSigma2",sigmaScale_fn,params=[sigma2, sigmaScale])
scaledSigma22 = zfit.ComposedParameter("scaledSigma22",sigmaScale_fn,params=[sigma22, sigmaScale])

alpha_left2.floating = False
alpha_right2.floating = False
n_left2.floating = False
n_right2.floating = False

sigma32.floating = False
scaledSigma32 = zfit.ComposedParameter("scaledSigma32",sigmaScale_fn,params=[sigma32, sigmaScale])
fracGaus2.floating = False
fracCB2.floating = False
fracCB2.floating = False

cb12_fit = zfit.pdf.CrystalBall(obs=obs, mu=shiftedMean2, sigma=scaledSigma2, alpha=alpha_left2, n=n_left2)
cb22_fit = zfit.pdf.CrystalBall(obs=obs, mu=shiftedMean2, sigma=scaledSigma22, alpha=alpha_right2, n=n_right2)
sigGaus2_fit = zfit.pdf.Gauss(obs=obs, mu=shiftedMean2, sigma=scaledSigma32)

sigModelCB2_fit = zfit.pdf.SumPDF([cb12_fit, cb22_fit], [fracCB2])
sigModel2_fit = zfit.pdf.SumPDF([sigModelCB2_fit,sigGaus2_fit],[fracGaus2])




frac0gamma_fit = zfit.Parameter("frac0gamma_fit", f0g, floating=True)
frac1gamma_fit = zfit.Parameter("frac1gamma_fit", f1g, floating=True)
frac0gammaConstraint = zfit.constraint.GaussianConstraint(frac0gamma_fit, observation=f0g , uncertainty=f0g*0.01 )
frac1gammaConstraint = zfit.constraint.GaussianConstraint(frac1gamma_fit, observation=f1g , uncertainty=f1g*0.01 )


sigModelTot_fit = zfit.pdf.SumPDF([sigModel0_fit, sigModel1_fit, sigModel2_fit], [frac0gamma_fit, frac1gamma_fit])
sigYield=zfit.Parameter('sigYield', nSig, 0, nTot)
sigModelTot_ext=sigModelTot_fit.create_extended(sigYield)

def prcYield_fn(sigYield, fracPrc):
    
    return sigYield*fracPrc
    

fracPrcVal=((BFKstee*effTotKstee)/(BFKee*effTotKee))
print("***********************************************")
print("Frac rare prc = {0}".format(fracPrcVal))
print("***********************************************")

fracPrc=zfit.Parameter('fracPrc_fit', fracPrcVal, floating=True)
prcYield = zfit.ComposedParameter('prcYield', prcYield_fn, params=[fracPrc,sigYield])
part_reco.set_yield(prcYield)
prc_ext = part_reco


prcConstraint = zfit.constraint.GaussianConstraint(fracPrc, observation=fracPrcVal , uncertainty=fracPrcVal*0.50 )


lam_fit = zfit.Parameter('lambda_fit', -1e-3, -0.1, 0.001)
comb_bkg_fit = zfit.pdf.Exponential(lam_fit, obs=obs)
combYield=zfit.Parameter("combYield", nCombi, 0, nTot)
comb_bkg_fit_ext=comb_bkg_fit.create_extended(combYield)


# values = z.unstack_x(data)
# obs_right_tail = zfit.Space('B_plus_M', (5400, 6200))
# data_tail = zfit.Data.from_tensor(obs=obs_right_tail, tensor=values)
# with comb_bkg.set_norm_range(obs_right_tail):
#     nll_tail = zfit.loss.UnbinnedNLL(comb_bkg, data_tail)
#     minimizer.minimize(nll_tail)
# lam.floating = False



modelTot_ext = zfit.pdf.SumPDF([sigModelTot_ext, comb_bkg_fit_ext, prc_ext])
# nll_ext = zfit.loss.ExtendedUnbinnedNLL(modelTot_ext, data, constraints=[frac0gammaConstraint,frac1gammaConstraint, prcConstraint])
nll_ext = zfit.loss.ExtendedUnbinnedNLL(modelTot_ext, data, constraints=[frac0gammaConstraint,frac1gammaConstraint])

print("***********************************************")
print("Fitting the toy dataset"						   )
print("***********************************************")


result = minimizer.minimize(nll_ext)


print(result.params)


print("***********************************************")
print("Done! getting the results...                   ")
print("***********************************************")

print("***********************************************")
print("Pulls:                                          ")
print("***********************************************")


fitResults={}
for item in result.hesse().items():
    fitResults['BDTCut']=cut
    if item[0].name=='sigYield':
        fittedVal=np.abs(item[0].value().numpy())
        fittedError=item[1]['error']
        genVal=np.abs(NKee)
        pull = (genVal-fittedVal)/fittedError
        print(item[0].name+' '+str(np.around(pull, decimals=3)))
        fitResults['sigYield']=fittedVal
        fitResults['sigYieldErr']=fittedError
        fitResults['sig_pull']=pull
        
    if item[0].name=='combYield':
        fittedVal=np.abs(item[0].value().numpy())
        fittedError=item[1]['error']   
        genVal=np.abs(Ncomb)
        pull=(genVal-fittedVal)/fittedError
        print(item[0].name+' '+str(np.around(pull, decimals=3)))     
        fitResults['combYield']=fittedVal     
        fitResults['combYieldErr']=fittedError
        fitResults['comb_pull']=pull
        
    if item[0].name=='fracPrc_fit':
        fittedVal=np.abs(item[0].value().numpy())
        fittedError=item[1]['error']
        genVal=np.abs(fracPrcVal)
        pull = (genVal-fittedVal)/fittedError                
        print(item[0].name+' '+str(np.around(pull, decimals=3)))
        fitResults['fracPrc_fit']=fittedVal     
        fitResults['fracPrc_fitErr']=fittedError
        fitResults['fracPrc_pull']=pull
                
    if item[0].name=='frac0gamma_fit':
        fittedVal=np.abs(item[0].value().numpy())
        fittedError=item[1]['error']
        genVal=np.abs(frac0gamma.value().numpy())
        pull = (genVal-fittedVal)/fittedError
        print(item[0].name+' '+str(np.around(pull, decimals=3)))
        fitResults['frac0gamma_fit']=fittedVal     
        fitResults['frac0gamma_fitErr']=fittedError
        fitResults['frac0gamma_pull']=pull
        
    if item[0].name=='frac1gamma_fit':
        fittedVal=np.abs(item[0].value().numpy())
        fittedError=item[1]['error']
        genVal=np.abs(frac1gamma.value().numpy())
        pull=(genVal-fittedVal)/fittedError
        print(item[0].name+' '+str(np.around(pull, decimals=3)))
        fitResults['frac1gamma_fit']=fittedVal     
        fitResults['frac1gamma_fitErr']=fittedError
        fitResults['frac1gamma_pull']=pull


    if item[0].name=='meanShift':
        fittedVal=np.abs(item[0].value().numpy())
        fittedError=item[1]['error']
        genVal=np.abs(shift)
        pull = (genVal-fittedVal)/fittedError 
        print(item[0].name+' '+str(np.around(pull, decimals=3)))
        fitResults['meanShift']=fittedVal    
        fitResults['meanShiftErr']=fittedError
        fitResults['meanShift_pull']=pull
        
    if item[0].name=='sigmaScale':
        fittedVal=np.abs(item[0].value().numpy())
        fittedError=item[1]['error']
        genVal=np.abs(scale)
        pull=(genVal-fittedVal)/fittedError         
        print(item[0].name+' '+str(np.around(pull, decimals=3)))
        fitResults['sigmaScale']=fittedVal    
        fitResults['sigmaScaleErr']=fittedError
        fitResults['sigmaScale_pull']=pull
        
    if item[0].name=='lambda_fit':
        fittedVal=np.abs(item[0].value().numpy())
        fittedError=item[1]['error']
        genVal=np.abs(lam.value().numpy())
        pull=(genVal-fittedVal)/fittedError         
        print(item[0].name+' '+str(np.around(pull, decimals=3)))
        fitResults['lambda_fit']=fittedVal    
        fitResults['lambda_fitErr']=fittedError
        fitResults['lambda_pull']=pull  


plot_comp_model(modelTot_ext,data, save_plot=True, fileName=plotdir+'plotTotalFit_BDT'+str(foldernum)+'.png')
        
with open(plotdir+'fit_results_'+str(foldernum)+'.pickle', 'wb') as handle:
    pickle.dump(fitResults, handle, protocol=pickle.HIGHEST_PROTOCOL)

# with open(plotdir+'zfit_results_'+str(foldernum)+'.pickle', 'wb') as handle:
#     pickle.dump(result, handle, protocol=pickle.HIGHEST_PROTOCOL)





