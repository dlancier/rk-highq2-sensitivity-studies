WHICHMAIN=(1)
# TRIGCATS=(1 2 3 4)
CUTLIST=(0. 0.22247613 0.3548028  0.44931267 0.52288402
       0.58313252 0.6341515  0.67839666 0.71745771 0.75242306
       0.78407068 0.81297598 0.83957654 0.86421291 0.88715533
       0.90862182 0.92879086 0.9478104  0.96580442 0.98287788)
# CUTLIST=(0.)
len=${#CUTLIST[@]}
	

for (( i=0; i<$len; i++ )); 
do

	echo "./create_BDT_study_scripts.sh ${CUTLIST[$i]}"
	./create_BDT_study_scripts.sh  sensitivity_studies_v1.py ${CUTLIST[$i]} $i

done

