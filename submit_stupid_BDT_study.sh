WHICHMAIN=(1)
# TRIGCATS=(1 2 3 4)
CUTLIST=(0.46275643 0.52288402 0.57381634 0.61799634 0.65700661
       0.69193128 0.72354557 0.75242306 0.77900008 0.80361626
       0.82654116 0.84799232 0.86814783 0.88715533 0.90513857
       0.92220233 0.93843607 0.95391682 0.96871133 0.98287788)
# CUTLIST=(0.)
len=${#CUTLIST[@]}


for (( i=0; i<$len; i++ )); 
do

	python sensitivity_studies_v1.py ${CUTLIST[$i]} $i &> "out/sensitivity_study_"$i".txt"

done


