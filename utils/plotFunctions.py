import matplotlib.pyplot as plt
from matplotlib import pylab
import zfit
from zfit import z
import numpy as np
import tensorflow as tf
import mplhep
mplhep.set_style('LHCb2')

def plot_model(model, data, scale=1, plot_data=True, save_plot=False, hidey=True, fileName=None):  # we will use scale later on

    nbins = 100
    data_plot = zfit.run(z.unstack_x(data))
    
    weights_set=None
    sumW=1.
    
    if data.has_weights:
        weights_set=data.weights.numpy()
        sumW=weights_set.sum()/len(weights_set)
        
    hist=np.histogram(data_plot, weights=weights_set, bins=nbins)[1]
    binw=hist[1]-hist[0]
    
    n_evts=len(data_plot)
    
    lower, upper = data.data_range.limit1d

    scale2=1.
    if(data.space.limits!=model.space.limits): scale2=model.integrate(data.space.limits)

    x = tf.linspace(lower, upper, num=1000)  
    y = model.pdf(x)* n_evts * binw * sumW /scale2
    y *= scale
    plt.plot(x, y)
      
    if plot_data:
        mplhep.histplot(plt.hist(data_plot, weights=weights_set, bins=nbins, facecolor="none"), 
                yerr=True, color='black', histtype='errorbar')
        if hidey:
            frame=pylab.gca()
            frame.axes.get_yaxis().set_ticks([])
        if save_plot:
            plt.savefig(fileName, dpi=200, format='png')
            plt.clf()
        else:
            pylab.show()
        # plt.saveAs
    

def plot_comp_model(model, data, save_plot=None, fileName=None):
    for mod, frac in zip(model.pdfs, model.params.values()):
        plot_model(mod, data, scale=frac, plot_data=False)
    plot_model(model, data, plot_data=True, save_plot=save_plot, fileName=fileName)
    

def plot_model_combi(model, data, scale=1, plot_data=True, save_plot=False, fileName=None):  # we will use scale later on

    nbins = 100
    data_plot = zfit.run(z.unstack_x(data))
    
    hist=np.histogram(data_plot, bins=nbins)[1]
    binw=hist[1]-hist[0]
    
    scale=1./model.integrate(limits=(5400,6200))
    
    n_evts=len(data_plot)
    for lim in data.data_range:
        lower, upper = lim.limit1d
        x = tf.linspace(lower, upper, num=1000)  # np.linspace also works
        y = model.pdf(x)* n_evts * binw
        y *= scale
        plt.plot(x, y)
     
    if plot_data:
            mplhep.histplot(plt.hist(data_plot, bins=nbins, facecolor="none"), 
                yerr=True, color='black', histtype='errorbar')
            frame = pylab.gca()
            frame.axes.get_yaxis().set_ticks([])

            if save_plot:
                plt.savefig(fileName, dpi=200, format='png')
                plt.clf()
                
            else:
                pylab.show()


branches = branches = [
            "eventNumber",    
            "nSPDHits",
            "e_plus_PIDe",
            "e_minus_PIDe",
            "e_plus_P",
            "e_minus_P",
            "e_plus_PT",
            "e_minus_PT",
            "e_plus_IPCHI2_OWNPV",
            "e_minus_IPCHI2_OWNPV",
            "J_psi_1S_M",
            "B_plus_M",
            "B_plus_DTFM_M",
            "K_Kst_MC15TuneV1_ProbNNk",
            "K_Kst_PIDK",
            "K_Kst_TRACK_GhostProb",
            "e_plus_TRACK_GhostProb",
            "e_minus_TRACK_GhostProb",
            "e_plus_hasRich",
            "e_plus_hasCalo",
            "e_minus_hasRich",
            "e_minus_hasCalo",
            # "dataCondSPDWeight",
            "K_Kst_P",
            "K_Kst_PE",
            # "poissonWeight",
            # "nPoissonWeights",
            "e_minus_PT",
            "e_minus_P",
            "e_plus_PT",
            "e_plus_P",
            "*HasBremAdded*",
            "L0Data_Muon1_Pt",
            "Polarity",
            "*_TRACK_PT",
            "passTrigCat*",
            "J_psi_1S_TRACK_M",
            "*hasRich*",
            "*L0Calo_ECAL_*Projection",
            "*DLL*",
            "K_Kst_PIDe",
            "Kemu_MKl",
            "Kemu_TRACK_MKl_l2pi",
            "Kemu_MKl_l2pi",
            "B_plus_M02",
            "BDT*",
            "KinWeight*",
            "B_plus_Hlt2Topo2*_TOS",
            "B_plus_Hlt2Topo3*_TOS",
            "*Hlt1TrackMVADecision_TOS",
            "*Hlt1TrackMVADecision_TOS",
            "*IPCHI2_OWNPV",
            "TCKCat",
            ]






            